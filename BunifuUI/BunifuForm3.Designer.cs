﻿namespace BunifuUI
{
    partial class BunifuForm3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BunifuForm3));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.p_close = new System.Windows.Forms.PictureBox();
            this.p_min = new System.Windows.Forms.PictureBox();
            this.p_max = new System.Windows.Forms.PictureBox();
            this.p_mini = new System.Windows.Forms.PictureBox();
            this.bunifuDatepicker1 = new Bunifu.Framework.UI.BunifuDatepicker();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_dat = new System.Windows.Forms.Label();
            this.bunifuImageButton1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuProgressBar1 = new Bunifu.Framework.UI.BunifuProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.bunifuRating1 = new Bunifu.Framework.UI.BunifuRating();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.bunifuSlider1 = new Bunifu.Framework.UI.BunifuSlider();
            this.lbl_value = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuTileButton1 = new Bunifu.Framework.UI.BunifuTileButton();
            this.bunifuTrackbar1 = new Bunifu.Framework.UI.BunifuTrackbar();
            this.lbl_vol = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuVTrackbar1 = new Bunifu.Framework.UI.BunifuVTrackbar();
            this.bunifuVTrackbar2 = new Bunifu.Framework.UI.BunifuVTrackbar();
            this.bunifuVTrackbar3 = new Bunifu.Framework.UI.BunifuVTrackbar();
            this.lbl_bass = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.lbl_treble = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.lbl_eq = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.p_close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p_mini)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 2;
            this.bunifuElipse1.TargetControl = this;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.lbl_dat);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(938, 152);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.panel1;
            this.bunifuDragControl1.Vertical = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.p_mini);
            this.panel2.Controls.Add(this.p_max);
            this.panel2.Controls.Add(this.p_min);
            this.panel2.Controls.Add(this.p_close);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(938, 36);
            this.panel2.TabIndex = 0;
            // 
            // p_close
            // 
            this.p_close.Dock = System.Windows.Forms.DockStyle.Right;
            this.p_close.Image = ((System.Drawing.Image)(resources.GetObject("p_close.Image")));
            this.p_close.Location = new System.Drawing.Point(907, 0);
            this.p_close.Name = "p_close";
            this.p_close.Size = new System.Drawing.Size(31, 36);
            this.p_close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.p_close.TabIndex = 0;
            this.p_close.TabStop = false;
            this.p_close.Click += new System.EventHandler(this.p_close_Click);
            // 
            // p_min
            // 
            this.p_min.Dock = System.Windows.Forms.DockStyle.Right;
            this.p_min.Image = ((System.Drawing.Image)(resources.GetObject("p_min.Image")));
            this.p_min.Location = new System.Drawing.Point(876, 0);
            this.p_min.Name = "p_min";
            this.p_min.Size = new System.Drawing.Size(31, 36);
            this.p_min.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.p_min.TabIndex = 1;
            this.p_min.TabStop = false;
            this.p_min.Click += new System.EventHandler(this.p_min_Click);
            // 
            // p_max
            // 
            this.p_max.Dock = System.Windows.Forms.DockStyle.Right;
            this.p_max.Image = ((System.Drawing.Image)(resources.GetObject("p_max.Image")));
            this.p_max.Location = new System.Drawing.Point(845, 0);
            this.p_max.Name = "p_max";
            this.p_max.Size = new System.Drawing.Size(31, 36);
            this.p_max.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.p_max.TabIndex = 2;
            this.p_max.TabStop = false;
            this.p_max.Visible = false;
            // 
            // p_mini
            // 
            this.p_mini.Dock = System.Windows.Forms.DockStyle.Right;
            this.p_mini.Image = ((System.Drawing.Image)(resources.GetObject("p_mini.Image")));
            this.p_mini.Location = new System.Drawing.Point(814, 0);
            this.p_mini.Name = "p_mini";
            this.p_mini.Size = new System.Drawing.Size(31, 36);
            this.p_mini.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.p_mini.TabIndex = 3;
            this.p_mini.TabStop = false;
            this.p_mini.Visible = false;
            // 
            // bunifuDatepicker1
            // 
            this.bunifuDatepicker1.BackColor = System.Drawing.Color.Black;
            this.bunifuDatepicker1.BorderRadius = 0;
            this.bunifuDatepicker1.ForeColor = System.Drawing.Color.White;
            this.bunifuDatepicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.bunifuDatepicker1.FormatCustom = "14:03:2017";
            this.bunifuDatepicker1.Location = new System.Drawing.Point(0, 152);
            this.bunifuDatepicker1.Name = "bunifuDatepicker1";
            this.bunifuDatepicker1.Size = new System.Drawing.Size(303, 36);
            this.bunifuDatepicker1.TabIndex = 1;
            this.bunifuDatepicker1.Value = new System.DateTime(2017, 3, 14, 10, 8, 26, 164);
            this.bunifuDatepicker1.onValueChanged += new System.EventHandler(this.bunifuDatepicker1_onValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Segoe UI Light", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Fuchsia;
            this.label1.Location = new System.Drawing.Point(0, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 50);
            this.label1.TabIndex = 1;
            this.label1.Text = "   Bunifu UI";
            // 
            // lbl_dat
            // 
            this.lbl_dat.AutoSize = true;
            this.lbl_dat.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dat.ForeColor = System.Drawing.Color.Fuchsia;
            this.lbl_dat.Location = new System.Drawing.Point(39, 86);
            this.lbl_dat.Name = "lbl_dat";
            this.lbl_dat.Size = new System.Drawing.Size(0, 19);
            this.lbl_dat.TabIndex = 2;
            // 
            // bunifuImageButton1
            // 
            this.bunifuImageButton1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.Image")));
            this.bunifuImageButton1.ImageActive = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.ImageActive")));
            this.bunifuImageButton1.Location = new System.Drawing.Point(110, 276);
            this.bunifuImageButton1.Name = "bunifuImageButton1";
            this.bunifuImageButton1.Size = new System.Drawing.Size(121, 110);
            this.bunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.bunifuImageButton1.TabIndex = 2;
            this.bunifuImageButton1.TabStop = false;
            this.bunifuImageButton1.Zoom = 10;
            this.bunifuImageButton1.Click += new System.EventHandler(this.bunifuImageButton1_Click);
            // 
            // bunifuProgressBar1
            // 
            this.bunifuProgressBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuProgressBar1.BorderRadius = 0;
            this.bunifuProgressBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bunifuProgressBar1.Location = new System.Drawing.Point(0, 504);
            this.bunifuProgressBar1.MaximumValue = 100;
            this.bunifuProgressBar1.Name = "bunifuProgressBar1";
            this.bunifuProgressBar1.ProgressColor = System.Drawing.Color.Black;
            this.bunifuProgressBar1.Size = new System.Drawing.Size(938, 10);
            this.bunifuProgressBar1.TabIndex = 3;
            this.bunifuProgressBar1.Value = 0;
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // bunifuRating1
            // 
            this.bunifuRating1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuRating1.ForeColor = System.Drawing.Color.Magenta;
            this.bunifuRating1.Location = new System.Drawing.Point(26, 84);
            this.bunifuRating1.Name = "bunifuRating1";
            this.bunifuRating1.Size = new System.Drawing.Size(126, 29);
            this.bunifuRating1.TabIndex = 3;
            this.bunifuRating1.Value = 0;
            this.bunifuRating1.onValueChanged += new System.EventHandler(this.bunifuRating1_onValueChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.bunifuRating1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(774, 36);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(164, 116);
            this.panel3.TabIndex = 4;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Light", 15F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Fuchsia;
            this.label2.Location = new System.Drawing.Point(50, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 28);
            this.label2.TabIndex = 5;
            this.label2.Text = "Rate us";
            // 
            // bunifuSlider1
            // 
            this.bunifuSlider1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSlider1.BackgroudColor = System.Drawing.Color.Silver;
            this.bunifuSlider1.BorderRadius = 2;
            this.bunifuSlider1.IndicatorColor = System.Drawing.Color.Black;
            this.bunifuSlider1.Location = new System.Drawing.Point(325, 158);
            this.bunifuSlider1.MaximumValue = 100;
            this.bunifuSlider1.Name = "bunifuSlider1";
            this.bunifuSlider1.Size = new System.Drawing.Size(357, 30);
            this.bunifuSlider1.TabIndex = 4;
            this.bunifuSlider1.Value = 0;
            this.bunifuSlider1.ValueChanged += new System.EventHandler(this.bunifuSlider1_ValueChanged);
            // 
            // lbl_value
            // 
            this.lbl_value.AutoSize = true;
            this.lbl_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_value.Location = new System.Drawing.Point(711, 158);
            this.lbl_value.Name = "lbl_value";
            this.lbl_value.Size = new System.Drawing.Size(18, 20);
            this.lbl_value.TabIndex = 5;
            this.lbl_value.Text = "0";
            // 
            // bunifuTileButton1
            // 
            this.bunifuTileButton1.BackColor = System.Drawing.Color.LightGray;
            this.bunifuTileButton1.color = System.Drawing.Color.LightGray;
            this.bunifuTileButton1.colorActive = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuTileButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTileButton1.Font = new System.Drawing.Font("Century Gothic", 11F);
            this.bunifuTileButton1.ForeColor = System.Drawing.Color.Black;
            this.bunifuTileButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuTileButton1.Image")));
            this.bunifuTileButton1.ImagePosition = 25;
            this.bunifuTileButton1.ImageZoom = 35;
            this.bunifuTileButton1.LabelPosition = 25;
            this.bunifuTileButton1.LabelText = "Settings";
            this.bunifuTileButton1.Location = new System.Drawing.Point(267, 276);
            this.bunifuTileButton1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bunifuTileButton1.Name = "bunifuTileButton1";
            this.bunifuTileButton1.Size = new System.Drawing.Size(127, 110);
            this.bunifuTileButton1.TabIndex = 6;
            this.bunifuTileButton1.Click += new System.EventHandler(this.bunifuTileButton1_Click);
            // 
            // bunifuTrackbar1
            // 
            this.bunifuTrackbar1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTrackbar1.BackgroudColor = System.Drawing.Color.DarkGray;
            this.bunifuTrackbar1.BorderRadius = 3;
            this.bunifuTrackbar1.IndicatorColor = System.Drawing.Color.Black;
            this.bunifuTrackbar1.Location = new System.Drawing.Point(110, 409);
            this.bunifuTrackbar1.MaximumValue = 100;
            this.bunifuTrackbar1.Name = "bunifuTrackbar1";
            this.bunifuTrackbar1.Size = new System.Drawing.Size(291, 30);
            this.bunifuTrackbar1.SliderRadius = 3;
            this.bunifuTrackbar1.TabIndex = 7;
            this.bunifuTrackbar1.Value = 0;
            this.bunifuTrackbar1.ValueChanged += new System.EventHandler(this.bunifuTrackbar1_ValueChanged);
            // 
            // lbl_vol
            // 
            this.lbl_vol.AutoSize = true;
            this.lbl_vol.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_vol.Location = new System.Drawing.Point(416, 411);
            this.lbl_vol.Name = "lbl_vol";
            this.lbl_vol.Size = new System.Drawing.Size(18, 20);
            this.lbl_vol.TabIndex = 8;
            this.lbl_vol.Text = "0";
            // 
            // bunifuVTrackbar1
            // 
            this.bunifuVTrackbar1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuVTrackbar1.BackgroudColor = System.Drawing.Color.DarkGray;
            this.bunifuVTrackbar1.BorderRadius = 3;
            this.bunifuVTrackbar1.IndicatorColor = System.Drawing.Color.Black;
            this.bunifuVTrackbar1.Location = new System.Drawing.Point(476, 287);
            this.bunifuVTrackbar1.MaximumValue = 20;
            this.bunifuVTrackbar1.Name = "bunifuVTrackbar1";
            this.bunifuVTrackbar1.Size = new System.Drawing.Size(30, 99);
            this.bunifuVTrackbar1.SliderRadius = 3;
            this.bunifuVTrackbar1.TabIndex = 9;
            this.bunifuVTrackbar1.Value = 10;
            this.bunifuVTrackbar1.ValueChanged += new System.EventHandler(this.bunifuVTrackbar1_ValueChanged);
            // 
            // bunifuVTrackbar2
            // 
            this.bunifuVTrackbar2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuVTrackbar2.BackgroudColor = System.Drawing.Color.DarkGray;
            this.bunifuVTrackbar2.BorderRadius = 3;
            this.bunifuVTrackbar2.IndicatorColor = System.Drawing.Color.Black;
            this.bunifuVTrackbar2.Location = new System.Drawing.Point(529, 287);
            this.bunifuVTrackbar2.MaximumValue = 20;
            this.bunifuVTrackbar2.Name = "bunifuVTrackbar2";
            this.bunifuVTrackbar2.Size = new System.Drawing.Size(30, 99);
            this.bunifuVTrackbar2.SliderRadius = 3;
            this.bunifuVTrackbar2.TabIndex = 10;
            this.bunifuVTrackbar2.Value = 12;
            this.bunifuVTrackbar2.ValueChanged += new System.EventHandler(this.bunifuVTrackbar2_ValueChanged);
            // 
            // bunifuVTrackbar3
            // 
            this.bunifuVTrackbar3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuVTrackbar3.BackgroudColor = System.Drawing.Color.DarkGray;
            this.bunifuVTrackbar3.BorderRadius = 3;
            this.bunifuVTrackbar3.IndicatorColor = System.Drawing.Color.Black;
            this.bunifuVTrackbar3.Location = new System.Drawing.Point(577, 287);
            this.bunifuVTrackbar3.MaximumValue = 20;
            this.bunifuVTrackbar3.Name = "bunifuVTrackbar3";
            this.bunifuVTrackbar3.Size = new System.Drawing.Size(30, 99);
            this.bunifuVTrackbar3.SliderRadius = 3;
            this.bunifuVTrackbar3.TabIndex = 11;
            this.bunifuVTrackbar3.Value = 10;
            this.bunifuVTrackbar3.ValueChanged += new System.EventHandler(this.bunifuVTrackbar3_ValueChanged);
            // 
            // lbl_bass
            // 
            this.lbl_bass.AutoSize = true;
            this.lbl_bass.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_bass.Location = new System.Drawing.Point(475, 254);
            this.lbl_bass.Name = "lbl_bass";
            this.lbl_bass.Size = new System.Drawing.Size(31, 13);
            this.lbl_bass.TabIndex = 12;
            this.lbl_bass.Text = "10db";
            // 
            // lbl_treble
            // 
            this.lbl_treble.AutoSize = true;
            this.lbl_treble.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_treble.Location = new System.Drawing.Point(528, 254);
            this.lbl_treble.Name = "lbl_treble";
            this.lbl_treble.Size = new System.Drawing.Size(31, 13);
            this.lbl_treble.TabIndex = 13;
            this.lbl_treble.Text = "12db";
            // 
            // lbl_eq
            // 
            this.lbl_eq.AutoSize = true;
            this.lbl_eq.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_eq.Location = new System.Drawing.Point(576, 254);
            this.lbl_eq.Name = "lbl_eq";
            this.lbl_eq.Size = new System.Drawing.Size(31, 13);
            this.lbl_eq.TabIndex = 14;
            this.lbl_eq.Text = "10db";
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(516, 394);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(50, 13);
            this.bunifuCustomLabel1.TabIndex = 15;
            this.bunifuCustomLabel1.Text = "Equalizer";
            // 
            // BunifuForm3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(938, 514);
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.lbl_eq);
            this.Controls.Add(this.lbl_treble);
            this.Controls.Add(this.lbl_bass);
            this.Controls.Add(this.bunifuVTrackbar3);
            this.Controls.Add(this.bunifuVTrackbar2);
            this.Controls.Add(this.bunifuVTrackbar1);
            this.Controls.Add(this.lbl_vol);
            this.Controls.Add(this.bunifuTrackbar1);
            this.Controls.Add(this.bunifuTileButton1);
            this.Controls.Add(this.lbl_value);
            this.Controls.Add(this.bunifuSlider1);
            this.Controls.Add(this.bunifuProgressBar1);
            this.Controls.Add(this.bunifuImageButton1);
            this.Controls.Add(this.bunifuDatepicker1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BunifuForm3";
            this.Text = "BunifuForm3";
            this.Load += new System.EventHandler(this.BunifuForm3_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.p_close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p_mini)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private System.Windows.Forms.PictureBox p_max;
        private System.Windows.Forms.PictureBox p_min;
        private System.Windows.Forms.PictureBox p_close;
        private System.Windows.Forms.PictureBox p_mini;
        private Bunifu.Framework.UI.BunifuDatepicker bunifuDatepicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_dat;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton1;
        private Bunifu.Framework.UI.BunifuProgressBar bunifuProgressBar1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel3;
        private Bunifu.Framework.UI.BunifuRating bunifuRating1;
        private System.Windows.Forms.Label label2;
        private Bunifu.Framework.UI.BunifuSlider bunifuSlider1;
        private Bunifu.Framework.UI.BunifuCustomLabel lbl_value;
        private Bunifu.Framework.UI.BunifuTileButton bunifuTileButton1;
        private Bunifu.Framework.UI.BunifuCustomLabel lbl_vol;
        private Bunifu.Framework.UI.BunifuTrackbar bunifuTrackbar1;
        private Bunifu.Framework.UI.BunifuCustomLabel lbl_eq;
        private Bunifu.Framework.UI.BunifuCustomLabel lbl_treble;
        private Bunifu.Framework.UI.BunifuCustomLabel lbl_bass;
        private Bunifu.Framework.UI.BunifuVTrackbar bunifuVTrackbar3;
        private Bunifu.Framework.UI.BunifuVTrackbar bunifuVTrackbar2;
        private Bunifu.Framework.UI.BunifuVTrackbar bunifuVTrackbar1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BunifuUI
{
    public partial class BunifuForm2 : Form
    {
        public BunifuForm2()
        {
            InitializeComponent();

            //Welcome to Tutorial5
            //We are dealing with 3 controls 
            //1. BunifuSeparator
            //2. BunifuTransition
            //3. BunifuGauge Control


            //1. BunifuSeparator.
            //Usually used to show separation of controls as the name indicates.
            //Basically like this.
            //it can be vertical or horizontal depends on which one you want
            //


            //2. BunifuTransition
            // Used to animate panels / containers for amazing experience
            //Lets try using one/two.
            //Let's animate the panels
            //panel1 set visible to false -- this is panel to show.
            //panel2. set visible property to true - this is the panel to hide
            // from here you can change transition type/ animation type whichever you want
            // as shown
            //all good


            //BunifuGauge control
            //set properties
            //let's implement some of events
            //first let's add a timer
            //and see progress.
            //let's debug

            // all good yeah?

            ///Thank you for watching
            /// See you in the next one
            /// @amos ;-)
            /// cheers!!

           


        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            //hide panel
            bunifuTransition1.HideSync(panel2, true, null);
            //this acts like a switch between the two panels
            panel1.Visible = false ;
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            //implementing
            //show panel
            bunifuTransition1.ShowSync(panel1, true, null);
            //this acts like a switch between the two panels
            panel2.Visible = true;
        }

        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //implement progress
            if(bunifuGauge1.Value !=100)
            {
                bunifuGauge1.Value += 5;
            }
        }
    }
}

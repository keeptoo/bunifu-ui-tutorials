﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BunifuUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_MouseHover(object sender, EventArgs e)
        {
            label1.ForeColor = Color.Red;
        }

        private void label1_MouseLeave(object sender, EventArgs e)
        {
            label1.ForeColor = Color.White;
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;

        }



        private void timer1_Tick(object sender, EventArgs e)
        {
            if (bunifuCircleProgressbar1.Value!=100)
            {
                bunifuCircleProgressbar1.Value += 10;
                bunifuGauge1.Value += 10;
            }

        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {
            bunifuTransition1.ShowSync(panel1, true, null);
        }

        private void bunifuRange1_RangeChanged(object sender, EventArgs e)
        {
         
        }
    }
}

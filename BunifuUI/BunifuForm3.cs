﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BunifuUI
{
    public partial class BunifuForm3 : Form
    {
        public BunifuForm3()
        {
            InitializeComponent();

            // With all that setup
            //Welcome again
            // We are going to deal with several controls 
            //Enjoy ;-)

            //BunifuDatePicker
            //Choose custom if you want specific Value to be displayed on the DatePicker.

            //BunifuImageButton
            //What about?
            //Let's see

            //Set Image
            //and ImageActive 
            //That's about it.
            // Done


            //BunifuProgressBar
            //Let's see what it can do.
            //Yeah!! that's it 
            //We are done with BunifuProgressBar.

            //Up Next
            //BunifuRating
            //You are not Rated. 



            //BunifuSlider
            //Let's see how it works
            //ofcourse twea properties first to look dapper!
            //You are good to go.

            //BunifuTileButton
            //Cool huh?


            //BunifuTrackbar
            //What about?
            //Let's see
            //Done

            //BunifuVTrackbar
            //Basically a Vertical Trackbar
            //Let's tweak!
            //Nice huh? Cool Equalizer or maybe what you like. 
            //Cheers
            //See you in the next one

            //k33ptoo!!



        


        }

        private void p_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void p_min_Click(object sender, EventArgs e)
        {
            if(this.WindowState == FormWindowState.Maximized)
            {
               
                p_min.Image = p_max.Image;
                this.WindowState = FormWindowState.Normal;
            }
            else
                if(this.WindowState == FormWindowState.Normal)
            {
               
                this.WindowState = FormWindowState.Maximized;
                p_min.Image = p_mini.Image;
            }

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bunifuDatepicker1_onValueChanged(object sender, EventArgs e)
        {
            lbl_dat.Text = bunifuDatepicker1.Value.ToLongDateString();
        }

        private void BunifuForm3_Load(object sender, EventArgs e)
        {
            lbl_dat.Text = bunifuDatepicker1.Value.ToLongDateString();
        }

        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            //Implement Click event
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if(bunifuProgressBar1.Value !=100)
            {
                bunifuProgressBar1.Value += 1;
            }
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bunifuRating1_onValueChanged(object sender, EventArgs e)
        {
            MessageBox.Show("A " + bunifuRating1.Value + " Star? Thank you! For Rating us");
            //Collect Your Rating here ;-)
        }

        private void bunifuSlider1_ValueChanged(object sender, EventArgs e)
        {
            lbl_value.Text = bunifuSlider1.Value.ToString();

            //All good! 
        }

        private void bunifuTileButton1_Click(object sender, EventArgs e)
        {
            //implement click here
            MessageBox.Show("I'm a Tile");
        }

        private void bunifuTrackbar1_ValueChanged(object sender, EventArgs e)
        {
            lbl_vol.Text = bunifuTrackbar1.Value.ToString();
        }

        private void bunifuVTrackbar1_ValueChanged(object sender, EventArgs e)
        {
            lbl_bass.Text = bunifuVTrackbar1.Value.ToString()+"db";
        }

        private void bunifuVTrackbar2_ValueChanged(object sender, EventArgs e)
        {
            lbl_treble.Text = bunifuVTrackbar2.Value.ToString()+"db";
        }

        private void bunifuVTrackbar3_ValueChanged(object sender, EventArgs e)
        {
            lbl_eq.Text = bunifuVTrackbar3.Value.ToString() + "db";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BunifuUI
{
    public partial class BunifuForm1 : Form
    {
        public BunifuForm1()
        {
            InitializeComponent();
            // We need to stylize our form so we add BunifuElipse
            //Add BunifuGradientPanel for more styling.
            //Add BunifuDrag to make the form draggable since it is borderless and make sure you select the TargetControl
            //So first step of UI Design using Bunifu Framework is Done
            //Next step is adding more controls
            //Watch Runtime. - Pretty good huh!
            //See you on the next tutorial.


            //----------Covered Components and Controls ------

            //----BunifuDrag
            //----BunifuCustomLabel
            //----BunifuElipse
            //----BunifuGradientPanel


            // ------------End-------------


            //Continuation


            //Add BunifuCards as a container for your contents - This Control has less customization - so we are good with that
            //Add BunifuFlatButton -Customize it
            //-- Note - Always sort your Properties Categorically for easy Navigation to Custom properties
            // We will use Jpicker to select best colors for the Button
            // Lets check it out. - all Good ;-)

            //Change Normal Color to Transparent to merge with background

            // - Making BunifuFlatButton as a Tab ---
            //Set isTab Property to True. -A button is selected by and set to Active.

            // Tweak Icons and Text -- All Good -Now lets check it out - Pretty Nice.

            // You can have your Icon to be right or left depending on your preference - That way.
            // You can also set the first Button to be selected..

            //Other properties - Border Radius - TextAlign - Other properties as indicated -




            //----------Covered Components and Controls ------

            // ----BunifuFlatButton
            //-----BunifuCards

            //-------------End ---------



            // See you in the next Video.


            //------------------------Tutorial 3 --------------

            //Welcome to tutorial 3
            //Let us add BunifuCircleProgressbar
            //You can change properties to suit your needs
            //Like Animating the CircleProgressBar
            //Better
            //Let's modify the size
            //Everything good.

            //BunifuDropDown
            //Customization is as simple as is
            //Let us populate items
            //Set Selectedindex to zero such that the first item shows on the dropdown

            //You can add items by calling .AddItem as you have seen.
            //Use the functions highlighted to perform various tasks - as the fuction name describes it

            //All good for BunifuDropDown

            //Cheers

            //BunifuMaterialTextbox

            //change all the properties to suit your design
            //To allow password character - set isPassword property to true

            //Textbox does not have so much to tweak so you are basically done with it.
            //

            //Thank you for watching - see you in the next tutorial



            //-----------------Tutorial 4 ---------------------------

            //Welcome _ today we are dealing with several controls.
            //1. Switches - BunifuSwitch & BunifuIOSSWitch
            //2. BunifuThinButton
            // That should be it.
            // Lets start.


            //BunifuSwitch 
            //Change color
            //Switch between values

            //BunifuIOSSwitch
            // Very simple isn't it. ;-)
            //


            //BunifuThinButton
            //if you experience render graphics - you can just close the form and re-open like this.
            //Good yeah.
            //Let's change some properties
            //That's about it.

            //cheers.







        }

        private void bunifuGradientPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bunifuCards1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bunifuCircleProgressbar1_ProgressChanged(object sender, EventArgs e)
        {

        }


        private void bunifuFlatButton5_Click(object sender, EventArgs e)
        {
            bunifuDropdown1.AddItem("Item 6");
        }

        private void bunifuCustomLabel1_Click(object sender, EventArgs e)
        {



        }

        private void BunifuForm1_Load(object sender, EventArgs e)
        {
            bunifuFlatButton1.selected = true; 
        }

        private void bunifuSwitch1_Click(object sender, EventArgs e)
        {

            if (bunifuSwitch1.Value == true)
            {
                switcher.Text = "True/On";
            }
            else
                switcher.Text = "False/Off";
        }

        private void bunifuiOSSwitch1_OnValueChange(object sender, EventArgs e)
        {
            if (bunifuiOSSwitch1.Value == true)
            {
                switcher.Text = "True/On";
            }
            else
                switcher.Text = "False/Off";
        }

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            bunifuThinButton21.ButtonText = "Clicked";
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
           // bunifuMaterialTextbox1.Select();
            string[] items = { "item - 1", "item - 2", "item - 3" };
            foreach (var item in items)
            {
                bunifuDropdown1.AddItem(item);
                bunifuFlatButton1.selected = true;

            }

        }

        private void bunifuDropdown1_onItemSelected(object sender, EventArgs e)
        {

        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            
        }

        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {

        }
    }
}
